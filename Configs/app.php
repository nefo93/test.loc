<?php

return [
    'Helpers/*.php',
    'app/Controllers/*.php',
    'Routes/route.php',
    'database/DB.php',
];

<?php

use function DI\create;
use function DI\autowire;

return [
    // Bind an interface to an implementation
    \App\Services\ImportService\ImportServiceInterface::class => autowire(\App\Services\ImportService\ImportService::class),
    \App\Services\FileService\FileServiceInterface::class => autowire(\App\Services\FileService\FileService::class),
    \App\Services\StorageService\StorageServiceInterface::class => create(\App\Services\StorageService\StorageService::class),
    \App\Services\ValidateService\ValidateServiceInterface::class => create(\App\Services\ValidateService\ValidateService::class),
    \App\DTO\InsertionData::class => create(\App\DTO\InsertionData::class),
    \App\DTO\ReportFields::class => create(\App\DTO\ReportFields::class),
];

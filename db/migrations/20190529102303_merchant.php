<?php

use Phinx\Migration\AbstractMigration;

class Merchant extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('merchant', ['id' => false, 'primary_key' => ['mid']]);
        $table->addColumn('mid', 'decimal',['precision' => 19, 'scale' => 1])
            ->addColumn('dba','string',['limit' => 100])
            ->create();
    }
    
    public function down()
    {
        $this->table('merchant')->drop()->save();
    }
}

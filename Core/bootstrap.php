<?php

use DI\ContainerBuilder;

$containerBuilder = new ContainerBuilder;
$containerBuilder->addDefinitions(__DIR__ . '/../Configs/config.php');
$container = $containerBuilder->build();

return $container;

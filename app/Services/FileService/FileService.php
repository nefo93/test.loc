<?php

namespace App\Services\FileService;

use App\Services\StorageService\StorageServiceInterface;
use App\Services\ValidateService\ValidateServiceInterface;
use App\DTO\ReportFields;
use \Exception;

class FileService implements FileServiceInterface
{
    /** @var StorageServiceInterface */
    protected $storageService;
    
    /** @var ValidateServiceInterface */
    protected $validateService;
    
    /** @var array mapping */
    protected $fieldsMapping;
    
    /** @var ReportFields */
    protected $reportFields;
    
    /** @var resource File */
    protected $file;
    
    /**
     * FileService constructor.
     * @param StorageServiceInterface $storageService
     * @param ValidateServiceInterface $validateService
     * @param ReportFields $reportFields
     */
    public function __construct(
        StorageServiceInterface $storageService,
        ValidateServiceInterface $validateService,
        ReportFields $reportFields
    )
    {
        $this->storageService = $storageService;
        $this->validateService = $validateService;
        $this->reportFields = $reportFields;
    }
    
    /**
     * @param string $url
     * @param array $mapping
     * @throws Exception
     */
    public function processFile(string $url, array $mapping): void
    {
        $this->file = $this->storageService->getFile($url);
        
        if (!$this->validateService->check($this->file, $mapping)) {
            throw new Exception('File is not valid. Some fields are missing.');
        }
        
        $this->fieldsMapping = $this->setColumnMapping($mapping);
    }
    
    /**
     * @param array $mapping
     * @return array
     */
    protected function setColumnMapping(array $mapping): array
    {
        $result = [];
        
        fseek($this->file, 0, SEEK_SET);
        
        if ($row = fgetcsv($this->file)) {
            foreach ($mapping as $column => $field) {
                $result[$column] = array_search($field, $row);
            }
        }
        
        return $result;
    }
    
    /**
     * @return ReportFields|null
     */
    public function getRow(): ?ReportFields
    {

        // it gets one row from file and leave cursor on the next line
        // so, we can use this method step by step (by iterations)
        if ($row = fgetcsv($this->file)) {
            $result = $this->reportFields->reportFields();
            foreach ($this->fieldsMapping as $column => $field) {
                $result->$column = $row[$field];
            }
        }
        
        return $result;
    }
}
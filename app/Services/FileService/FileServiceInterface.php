<?php

namespace App\Services\FileService;

interface FileServiceInterface
{
    public function processFile(string $url, array $fields): void;
}
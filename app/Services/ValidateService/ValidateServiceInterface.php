<?php

namespace App\Services\ValidateService;


interface ValidateServiceInterface
{
    public function check($file, array $mapping): bool;
}
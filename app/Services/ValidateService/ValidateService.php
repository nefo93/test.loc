<?php

namespace App\Services\ValidateService;


class ValidateService implements ValidateServiceInterface
{
    /**
     * @param $file
     * @param array $mapping
     * @return bool
     */
    public function check($file, array $mapping): bool
    {
        fseek($file, 0, SEEK_SET);
    
        $fields = fgetcsv($file);
        
        foreach ($mapping as $value) {
            if (!in_array($value, $fields)) {
                return false;
            }
        }
    
        return true;
    }
}
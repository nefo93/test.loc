<?php

namespace App\Services\StorageService;

use \Exception;

class StorageService implements StorageServiceInterface
{
    const FILE_NAME = 'report.csv';
    
    const STORAGE_PATH = __DIR__.'/../../../Resources/Storage/';
    
    /**
     * @param string $url
     * @return bool|mixed|resource
     * @throws Exception
     */
    public function getFile(string $url)
    {
        set_time_limit(0);
        
        $fileWrite = fopen(self::STORAGE_PATH . self::FILE_NAME, 'w+');

        $fileRead = fopen($url, 'r');
        if ($fileRead) {
            while (($line = fgets($fileRead)) !== false) {
                fwrite($fileWrite, $line);
            }

            fclose($fileRead);
            fclose($fileWrite);

            return fopen(self::STORAGE_PATH . self::FILE_NAME, 'r');
        }

        throw new Exception('Can\'t open file');
    }
}
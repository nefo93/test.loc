<?php

namespace App\Services\StorageService;

interface StorageServiceInterface
{
    public function getFile(string $url);
}
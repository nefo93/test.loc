<?php

namespace App\Services\ImportService;

use App\DTO\ReportFields;
use App\Services\FileService\FileService;
use App\DTO\InsertionData;
use \Exception;

class ImportService implements ImportServiceInterface
{
    /** @var FileService */
    protected $fileService;
    
    /** @var InsertionData */
    protected $insertionData;
    
    /** @var ReportFields */
    protected $bufferRow;
    
    /**
     * ImportService constructor.
     * @param FileService $fileService
     * @param InsertionData $insertionData
     */
    public function __construct(FileService $fileService, InsertionData $insertionData)
    {
        $this->fileService = $fileService;
        $this->insertionData = $insertionData;
    }
    
    /**
     * @param string $url
     * @param array $mapping
     */
    public function import(string $url, array $mapping): void
    {
        try {
            $this->fileService->processFile($url, $mapping);
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    
        while ($rows = $this->getRows()) {
            var_dump($rows);
        }
    }
    
    /**
     * @return InsertionData
     */
    private function getRows()
    {
        /** @var ReportFields $newRow */
        $newRow = $this->bufferRow ?? $this->fileService->getRow();
        
        if ($newRow) {
            $data = $this->insertionData->insertData();

            do {
                $row = $newRow;
                $row->getBatchFields();
                $data->setBatchFields($row->getBatchFields())
                    ->addTransaction(
                        $row->getMerchantFields(),
                        $row->getCardTypeFields(),
                        $row->getTransactionTypeFields(),
                        $row->getTransactionFields()
                    );

                $isSameBatch = false;
                $newRow = $this->fileService
                    ->getRow();

                if ($newRow) {
                    $isSameBatch = $this->checkRows($row, $newRow);
                }

            } while ($newRow && $isSameBatch);

            $this->bufferRow = $newRow;
        }
        return $data;
    }
    
    /**
     * @param ReportFields $row
     * @param ReportFields $newRow
     * @return bool
     */
    private function checkRows(
        ReportFields $row,
        ReportFields $newRow
    ): bool
    {
        return $newRow->getBatchDate() == $row->getBatchDate() &&
            $newRow->getBatchNumber() == $row->getBatchNumber();
    }
}
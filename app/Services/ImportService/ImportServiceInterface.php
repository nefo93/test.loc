<?php

namespace App\Services\ImportService;


interface ImportServiceInterface
{
    /**
     * @param string $url
     * @param array $mapping
     */
    public function import(string $url, array $mapping): void;
}
<?php
namespace App\Controllers;

use App\Services\ImportService\ImportServiceInterface;

class HomeController
{
    /** @var ImportServiceInterface */
    protected $importService;
    
    /** @var array Mapping */
    protected $mapping;
    
    /** @var string  path file */
    protected $fileLink;
    
    /**
     * HomeController constructor.
     * @param ImportServiceInterface $importService
     */
    public function __construct(ImportServiceInterface $importService)
    {
        $settings = config('settings');
        
        $this->mapping = $settings['mapping'];
        $this->fileLink = $settings['fileLink'];
        $this->importService = $importService;
    }
    
    public function index()
    {
        $this->importService->import($this->fileLink, $this->mapping);
    }
}